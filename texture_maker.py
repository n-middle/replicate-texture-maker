# Команда для установки необходимых библиотек
# pip install pyTelegramBotAPI requests Pillow replicate  

import os
import time
import telebot
import logging
import requests
import base64
from pprint import pprint
import random
from PIL import Image
import replicate
import io

# Вставьте ваш токен Replicate здесь
replicate_client = replicate.Client(api_token="Ваш токен Replicate")

# Вставьте ваш токен Telegram бота здесь
bot = telebot.TeleBot("Ваш токен телеграм бота")

# Здесь должны быть ID пользователей, имеющих доступ к боту через запятую 
users = "1111111, 2222222, 333333, 444444"

#------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------

# Create logger for bot
imagetypes = [".png",".jpg",".jpeg",".bmp",".tga",".dds"]
logging.basicConfig(filename='bot.log', level=logging.INFO)

ALLOWED_USERS = users.split(",")
ALLOWED_USERS = list(map(int, ALLOWED_USERS))

# Получаем путь к папке, где находится скрипт
script_path = os.path.dirname(__file__)

# Function to encode the image
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')

# Decorator function to check access
def restricted_access(func):
    def wrapper(message):

        user_id = message.from_user.id

        if user_id in ALLOWED_USERS:
            return func(message)
        else:
            bot.reply_to(message, "Извините, у вас нет доступа к этому боту")

    return wrapper

# Make texture
def mk_texture(message, mode=0):

    chat_id = message.chat.id
    user_id = message.chat.id

    wait_msg = bot.reply_to(message, 'Model is booting up.')

    text = message.text
    text = text[4:]

    resolution = [512, 512]

    # If specific style were given, use it
    bracket_str = text
    bracket_index_start = bracket_str.find("[")
    bracket_index_end = bracket_str.find("]")

    if bracket_index_start != -1 and bracket_index_end != -1:
        resolution = bracket_str[bracket_index_start + 1:bracket_index_end].split("x")
        text = bracket_str[bracket_index_end + 1:]

    print('Prompt: ' + text)

     # Соединяем путь с именем json файла
    user_img = str(user_id) + '.png'

    # Path to your image
    image_path = os.path.join(script_path, user_img)
    base64_image = None

    if os.path.exists(image_path) and mode == 1:

        with Image.open(image_path) as img:

            resized_img = img.resize((512, 512), Image.LANCZOS)
            width, height = resized_img.size

            print('Resized image: ' + str(width) + 'x' + str(height))

            # Сохранить картинку
            resized_img.save(image_path)

        # Getting the base64 string
        base64_image = encode_image(image_path)

    try:
        if mode == 0:
            output = replicate_client.run(
                "tstramer/material-diffusion:a42692c54c0f407f803a0a8a9066160976baedb77c91171a01730f9b0d7beeff",
                input={
                    "width": int(resolution[0]),
                    "height": int(resolution[1]),
                    "prompt": text,
                    "scheduler": "K-LMS",
                    "num_outputs": 1,
                    "guidance_scale": 7.5,
                    "prompt_strength": 0.8,
                    "num_inference_steps": 50
                }
            )
        else:
            output = replicate_client.run(
                "tstramer/material-diffusion:a42692c54c0f407f803a0a8a9066160976baedb77c91171a01730f9b0d7beeff",
                input={
                    "width": int(resolution[0]),
                    "height": int(resolution[1]),
                    "prompt": text,
                    "scheduler": "K-LMS",
                    "num_outputs": 1,
                    "init_image": f"data:image/jpeg;base64,{base64_image}",
                    "guidance_scale": 7.5,
                    "prompt_strength": 0.8,
                    "num_inference_steps": 50
                }
            )
        
        # Соединяем путь с именем json файла
        user_img = str(user_id) + '.png'
        image_path = os.path.join(script_path, user_img)

        # Download the image from the URL
        image_url = output[0]
        response = requests.get(image_url)

        # Save the image data to a file
        image_data = response.content
        image = Image.open(io.BytesIO(image_data))
        image.save(image_path)
        width, height = image.size

        # If image large, send it as document
        if width > 1024 or height > 1024:
            send_image(message, image_path)
        else:
            bot.send_photo(message.chat.id, photo=output[0])
        #send_image(message, image_path)

        bot.delete_message(message.chat.id, wait_msg.message_id)

    except Exception as e:
        print(e)
        bot.reply_to(message, str(e))

# Upscale image
def mk_upscale(message, model=0):

    select_model = 0

    chat_id = message.chat.id
    user_id = message.chat.id

    wait_msg = bot.reply_to(message, 'Model is booting up.')

    text = message.text
    text = text[4:]

    upscale_factor = 4

    # If specific style were given, use it
    bracket_str = text
    bracket_index_start = bracket_str.find("[")
    bracket_index_end = bracket_str.find("]")

    if bracket_index_start != -1 and bracket_index_end != -1:
        text = bracket_str[bracket_index_end + 1:]

    print('Prompt: ' + text)

     # Соединяем путь с именем json файла
    user_img = str(user_id) + '.png'

    # Path to your image
    image_path = os.path.join(script_path, user_img)
    base64_image = None

    if os.path.exists(image_path):

        with Image.open(image_path) as img:
            width, height = img.size

        if bracket_index_start != -1 and bracket_index_end != -1:
            target_size = bracket_str[bracket_index_start + 1:bracket_index_end]

            # Select upscale model
            if int(target_size) == 1:
                select_model = 1

            original_size = width
            if height > width:
                original_size = height

            upscale_factor = calculate_upscale_factor(original_size, target_size)

        # Getting the base64 string
        base64_image = encode_image(image_path)

        try:
            if select_model == 0:
                output = replicate_client.run(
                    "philz1337x/clarity-upscaler:abd484acb51ad450b06f42f76940fa5c1b37511dbf70ac8594fdacd5c3302307",
                    input={
                        "seed": 1337,
                        "image": f"data:image/jpeg;base64,{base64_image}",
                        "prompt": text,
                        "dynamic": 6,
                        "sd_model": "juggernaut_reborn.safetensors [338b85bc4f]",
                        "scheduler": "DPM++ 3M SDE Karras",
                        "creativity": 0.35,
                        "downscaling": False,
                        "resemblance": 0.6,
                        "scale_factor": upscale_factor,
                        "tiling_width": 112,
                        "tiling_height": 144,
                        "negative_prompt": "(worst quality, low quality, normal quality:2) JuggernautNegative-neg",
                        "num_inference_steps": 18,
                        "downscaling_resolution": 768
                    }
                )
            elif select_model == 1:
                output = replicate_client.run(
                    "jingyunliang/hcflow-sr:567785261e974455ede3b0644d1d7e5aa8d9e40a22217a8726900acdcd9e19ce",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image}",
                        "model_type": "general"
                    }
                )


            # Соединяем путь с именем json файла
            user_img = str(user_id) + '.png'
            image_path = os.path.join(script_path, user_img)

            # Download the image from the URL
            image_url = output[0]
            response = requests.get(image_url)

            # Save the image data to a file
            image_data = response.content
            image = Image.open(io.BytesIO(image_data))
            image.save(image_path)
            width, height = image.size

            # If image large, send it as document
            # if width > 1024 or height > 1024:
            #     send_image(message, image_path)
            # else:
            #     bot.send_photo(message.chat.id, photo=output)
            send_image(message, image_path)
            
            bot.delete_message(message.chat.id, wait_msg.message_id)

        except Exception as e:
            print(e)
            bot.reply_to(message, str(e))
    else:
        bot.reply_to(message, 'Send me an image first.')
        bot.delete_message(message.chat.id, wait_msg.message_id)

        
# Upscale material image
def mk_upscale_mat(message, model = 0):

    select_model = 0

    chat_id = message.chat.id
    user_id = message.chat.id

    # Соединяем путь с именем json файла
    user_img_normal = str(user_id) + '_normal.png'
    image_path_normal = os.path.join(script_path, user_img_normal)

    user_img_roughness = str(user_id) + '_roughness.png'
    image_path_roughness = os.path.join(script_path, user_img_roughness)

    user_img_displacement = str(user_id) + '_displacement.png'
    image_path_displacement = os.path.join(script_path, user_img_displacement)

    wait_msg = bot.reply_to(message, 'Model is booting up.')

    text = message.text
    text = text[7:]

    # If specific style were given, use it
    bracket_str = text
    bracket_index_start = bracket_str.find("[")
    bracket_index_end = bracket_str.find("]")

    if bracket_index_start != -1 and bracket_index_end != -1:
        text = bracket_str[bracket_index_end + 1:]

    print('Prompt: ' + text)

     # Соединяем путь с именем json файла
    user_img = str(user_id) + '.png'

    # Path to your image
    image_path = os.path.join(script_path, user_img)
    base64_image = None

    if os.path.exists(image_path_normal) and os.path.exists(image_path_roughness) and os.path.exists(image_path_displacement):

        width = 256
        height = 256

        # Resize image to 1024 if it larger
        target_size = 1024
        with Image.open(image_path_normal) as img:
            width, height = img.size
            new_width = width
            new_height = height

            #bot.reply_to(message, 'Image resized to ' + str(width))

            aspect_ratio = width / height
            resize = False

            if width > target_size or height > target_size:
                if width > height:
                    new_width = target_size
                    new_height = round(new_width / aspect_ratio)
                else:
                    new_height = target_size
                    new_width = round(new_height * aspect_ratio)
                resize = True

            if resize:
                resized_img = img.resize((new_width, new_height), Image.LANCZOS)
                width, height = resized_img.size

                print('Resized image: ' + str(width) + 'x' + str(height))

                # Сохранить картинку
                resized_img.save(image_path_normal)

        with Image.open(image_path_roughness) as img:
            width, height = img.size
            new_width = width
            new_height = height

            #bot.reply_to(message, 'Image resized to ' + str(width))

            aspect_ratio = width / height
            resize = False

            if width > target_size or height > target_size:
                if width > height:
                    new_width = target_size
                    new_height = round(new_width / aspect_ratio)
                else:
                    new_height = target_size
                    new_width = round(new_height * aspect_ratio)
                resize = True

            if resize:
                resized_img = img.resize((new_width, new_height), Image.LANCZOS)
                width, height = resized_img.size

                print('Resized image: ' + str(width) + 'x' + str(height))

                # Сохранить картинку
                resized_img.save(image_path_roughness)

        with Image.open(image_path_displacement) as img:
            width, height = img.size
            new_width = width
            new_height = height

            #bot.reply_to(message, 'Image resized to ' + str(width))

            aspect_ratio = width / height
            resize = False

            if width > target_size or height > target_size:
                if width > height:
                    new_width = target_size
                    new_height = round(new_width / aspect_ratio)
                else:
                    new_height = target_size
                    new_width = round(new_height * aspect_ratio)
                resize = True

            if resize:
                resized_img = img.resize((new_width, new_height), Image.LANCZOS)
                width, height = resized_img.size

                print('Resized image: ' + str(width) + 'x' + str(height))

                # Сохранить картинку
                resized_img.save(image_path_displacement)

        with Image.open(image_path_normal) as img:
            width, height = img.size

        if bracket_index_start != -1 and bracket_index_end != -1:
            target_size = bracket_str[bracket_index_start + 1:bracket_index_end]
            if target_size > 2048:
                target_size = 2048

            # Select upscale model
            if int(target_size) == 1:
                select_model = 1

            original_size = width
            if height > width:
                original_size = height

            upscale_factor = calculate_upscale_factor(original_size, target_size)

        # Getting the base64 string
        base64_image_normal = encode_image(image_path_normal)
        base64_image_roughness = encode_image(image_path_roughness)
        base64_image_displacement = encode_image(image_path_displacement)

        try:
            # Normal map upscale
            if select_model == 0:

                output = replicate_client.run(
                    "xpixelgroup/hat:ad47b01e4923c19fed424451d925d7e95b743be1df19e9b3b0dbb9ed8685ed6b",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image_normal}"
                    }
                )
                

            elif select_model == 1:
                output = replicate_client.run(
                    "jingyunliang/hcflow-sr:567785261e974455ede3b0644d1d7e5aa8d9e40a22217a8726900acdcd9e19ce",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image}",
                        "model_type": "general"
                    }
                )

            # Download the image from the URL
            image_url = output
            response = requests.get(image_url)

            # Save the image data to a file
            image_data = response.content
            image = Image.open(io.BytesIO(image_data))

            image.save(image_path_normal)
            send_image(message, image_path_normal)
            
            bot.delete_message(message.chat.id, wait_msg.message_id)

            # Roughness map upscale
            if select_model == 0:
                output = replicate_client.run(
                    "xpixelgroup/hat:ad47b01e4923c19fed424451d925d7e95b743be1df19e9b3b0dbb9ed8685ed6b",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image_roughness}"
                    }
                )


            elif select_model == 1:
                output = replicate_client.run(
                    "jingyunliang/hcflow-sr:567785261e974455ede3b0644d1d7e5aa8d9e40a22217a8726900acdcd9e19ce",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image}",
                        "model_type": "general"
                    }
                )

            # Download the image from the URL
            image_url = output
            response = requests.get(image_url)

            # Save the image data to a file
            image_data = response.content
            image = Image.open(io.BytesIO(image_data))
            image.save(image_path_roughness)
            send_image(message, image_path_roughness)

            # Displacment map upscale
            if select_model == 0:
                output = replicate_client.run(
                    "xpixelgroup/hat:ad47b01e4923c19fed424451d925d7e95b743be1df19e9b3b0dbb9ed8685ed6b",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image_displacement}"
                    }
                )

            elif select_model == 1:
                output = replicate_client.run(
                    "jingyunliang/hcflow-sr:567785261e974455ede3b0644d1d7e5aa8d9e40a22217a8726900acdcd9e19ce",
                    input={
                        "image": f"data:image/jpeg;base64,{base64_image}",
                        "model_type": "general"
                    }
                )

            # Download the image from the URL
            image_url = output
            response = requests.get(image_url)

            # Save the image data to a file
            image_data = response.content
            image = Image.open(io.BytesIO(image_data))
            image.save(image_path_displacement)
            send_image(message, image_path_displacement)

        except Exception as e:
            print(e)
            bot.reply_to(message, str(e))
    else:
        bot.reply_to(message, 'Сначала сгенерируйте карты Normal, Roughness, Displacment')
        bot.delete_message(message.chat.id, wait_msg.message_id)

# Make image with playground 2.5
def mk_img(message):
    wait_msg = bot.reply_to(message, 'Model is booting up.')

    text = message.text
    text = text[5:]

    resolution = [1152, 896]

    # If specific style were given, use it
    bracket_str = text
    bracket_index_start = bracket_str.find("[")
    bracket_index_end = bracket_str.find("]")

    if bracket_index_start != -1 and bracket_index_end != -1:
        resolution = bracket_str[bracket_index_start + 1:bracket_index_end].split("x")
        text = bracket_str[bracket_index_end + 1:]

    print('Resolution: ' + str(resolution))
    print('Prompt: ' + text)

    try:
        output = replicate_client.run(
            "konieshadow/fooocus-api-realistic:612fd74b69e6c030e88f6548848593a1aaabe16a09cb79e6d714718c15f37f47",
            input={
                "prompt": text,
                "cn_type1": "ImagePrompt",
                "cn_type2": "ImagePrompt",
                "cn_type3": "ImagePrompt",
                "cn_type4": "ImagePrompt",
                "sharpness": 2,
                "image_seed": random.randint(0, int(time.time())),
                "uov_method": "Disabled",
                "image_number": 1,
                "guidance_scale": 3,
                "refiner_switch": 0.5,
                "negative_prompt": "unrealistic, saturated, high contrast, big nose, watermark, signature, label",
                "style_selections": "Fooocus V2,Fooocus Photograph,Fooocus Negative",
                "uov_upscale_value": 0,
                "outpaint_selections": "",
                "outpaint_distance_top": 0,
                "performance_selection": "Speed",
                "outpaint_distance_left": 0,
                "aspect_ratios_selection": str(resolution[0]) + "*" + str(resolution[1]),
                "outpaint_distance_right": 0,
                "outpaint_distance_bottom": 0,
                "inpaint_additional_prompt": "",
                "disable_safety_checker": True
            }
        )
    
        bot.send_photo(message.chat.id, photo=output[0])
        bot.delete_message(message.chat.id, wait_msg.message_id)

    except Exception as e:
        print(e)
        bot.reply_to(message, str(e))

# Materialize texture
def mk_mat(message):

    chat_id = message.chat.id
    user_id = message.chat.id

    wait_msg = bot.reply_to(message, 'Model is booting up.')

    text = message.text
    text = text[4:]

    resolution = [512, 512]

    # If specific style were given, use it
    bracket_str = text
    bracket_index_start = bracket_str.find("[")
    bracket_index_end = bracket_str.find("]")

    if bracket_index_start != -1 and bracket_index_end != -1:
        resolution = bracket_str[bracket_index_start + 1:bracket_index_end].split("x")
        text = bracket_str[bracket_index_end + 1:]

    print('Resolution: ' + str(resolution))
    print('Prompt: ' + text)

     # Соединяем путь с именем json файла
    user_img = str(user_id) + '.png'

    # Path to your image
    image_path = os.path.join(script_path, user_img)
    base64_image = None

    if os.path.exists(image_path):

        width = 256
        height = 256

        # Resize image to 1024 if it larger
        with Image.open(image_path) as img:
            width, height = img.size
            new_width = width
            new_height = height

            #bot.reply_to(message, 'Image resized to ' + str(width))

            aspect_ratio = width / height
            resize = False
            target_size = 1024

            if width > target_size or height > target_size:
                if width > height:
                    new_width = target_size
                    new_height = round(new_width / aspect_ratio)
                else:
                    new_height = target_size
                    new_width = round(new_height * aspect_ratio)
                resize = True

            if resize:
                resized_img = img.resize((new_width, new_height), Image.LANCZOS)
                width, height = resized_img.size

                print('Resized image: ' + str(width) + 'x' + str(height))

                # Сохранить картинку
                resized_img.save(image_path)
                #bot.reply_to(message, 'Image resized to ' + str(width))


        with Image.open(image_path) as img:
            width, height = img.size
        # Getting the base64 string
        base64_image = encode_image(image_path)
        #bot.reply_to(message, 'Image tile will be ' + str(width))

        try:
            output = replicate_client.run(
                "midllle/material-maker:92fb3df0bf2a5f3bb60af26366677e0a98866ea5b8b5aa4a229f98322118c74e",
                input={
                    "input_image": f"data:image/jpeg;base64,{base64_image}",
                    "mirror": False,
                    "seamless": True,
                    "ishiiruka": False,
                    "replicate": False,
                    "tile_size": width,
                    "ishiiruka_texture_encoder": False
                }
            )

            # Соединяем путь с именем json файла
            user_img_normal = str(user_id) + '_normal.png'
            image_path_normal = os.path.join(script_path, user_img_normal)

            user_img_roughness = str(user_id) + '_roughness.png'
            image_path_roughness = os.path.join(script_path, user_img_roughness)

            user_img_displacement = str(user_id) + '_displacement.png'
            image_path_displacement = os.path.join(script_path, user_img_displacement)

            # Download the image from the URL
            image_url_normal = output[0]
            response_normal = requests.get(image_url_normal)

            image_url_roughness = output[1]
            response_roughness = requests.get(image_url_roughness)

            image_url_displacement = output[2]
            response_displacement = requests.get(image_url_displacement)

            # Save the image data to a file
            image_data_normal = response_normal.content
            image_normal = Image.open(io.BytesIO(image_data_normal))
            image_normal.save(image_path_normal)

            image_data_roughness = response_roughness.content
            image_roughness = Image.open(io.BytesIO(image_data_roughness))
            image_roughness.save(image_path_roughness)

            image_data_displacement = response_displacement.content
            image_displacement = Image.open(io.BytesIO(image_data_displacement))
            image_displacement.save(image_path_displacement)

            width, height = image_normal.size
            send_image(message, image_path_normal)
            send_image(message, image_path_roughness)
            send_image(message, image_path_displacement)

            bot.delete_message(message.chat.id, wait_msg.message_id)

        except Exception as e:
            print(e)
            bot.reply_to(message, str(e))
    else:
        bot.reply_to(message, 'Сначала пришлите картинку')
        bot.delete_message(message.chat.id, wait_msg.message_id)

        
# Make image with juggernaut
def mk_juggernaut(message):

    chat_id = message.chat.id
    user_id = message.chat.id

    wait_msg = bot.reply_to(message, 'Model is booting up.')

    # Замеряем время начала генерации
    startPredictionTime = time.time()

    text = message.text
    text = text[5:]

    resolution = [1152, 896]

    # If specific style were given, use it
    bracket_str = text
    bracket_index_start = bracket_str.find("[")
    bracket_index_end = bracket_str.find("]")

    if bracket_index_start != -1 and bracket_index_end != -1:
        resolution = bracket_str[bracket_index_start + 1:bracket_index_end].split("x")
        text = bracket_str[bracket_index_end + 1:]

    print('Resolution: ' + str(resolution))
    print('Prompt: ' + text)

    try:
        output = replicate_client.run(
            "lucataco/juggernaut-xl-v9:bea09cf018e513cef0841719559ea86d2299e05448633ac8fe270b5d5cd6777e",
            input={
                "width": int(resolution[0]),
                "height": int(resolution[1]),
                "prompt": text,
                "scheduler": "DPM++SDE",
                "num_outputs": 1,
                "guidance_scale": 2,
                "apply_watermark": False,
                "num_inference_steps": 5,
                "disable_safety_checker": True
            }
        )
        # Соединяем путь с именем json файла
        user_img = str(user_id) + '.png'
        image_path = os.path.join(script_path, user_img)

        # Download the image from the URL
        image_url = output[0]
        response = requests.get(image_url)

        # Save the image data to a file
        image_data = response.content
        image = Image.open(io.BytesIO(image_data))
        image.save(image_path)
        width, height = image.size

        # If image large, send it as document
        if width > 1024 or height > 1024:
            send_image(message, image_path)
        else:
            bot.send_photo(message.chat.id, photo=output[0])
    
        bot.delete_message(message.chat.id, wait_msg.message_id)

    except Exception as e:
        print(e)
        bot.reply_to(message, str(e))


# Send image as document
def send_image(message, image_path):
    try:
        file = open(image_path, 'rb')
        bot.send_document(message.chat.id, file)
        with Image.open(image_path) as img:
            width, height = img.size
            bot.reply_to(message, 'Image size: ' + str(width) + 'x' + str(height))
    except Exception as e:
        bot.reply_to(message, str(e))

def calculate_upscale_factor(original_size, target_size):
    # Рассчитываем downscaleFactor
    upscale_factor = int(target_size) / int(original_size)
    return upscale_factor

# Add a /help command handler
@bot.message_handler(commands=['start', 'help'])
def start_message(message):
    bot.reply_to(message, 'Вы можете генерировать текстуры с помощью /tex Asphalt with cracks')
    bot.reply_to(message, 'Вы можете сгенерировать карты Normal, Roughness, Displacement для текстуры с помощью команды /mat')
    bot.reply_to(message, 'Вы можете применить художественный апскейл командой /up [2048] Asphalt with cracks')
    bot.reply_to(message, 'Вы можете увеличить изображения Normal, Roughness, Displacement, сначала сгенерируйте их и затем используйте команду /upmat')
    bot.reply_to(message, 'Вы можете генерировать художественные изображения, используя команду /img')
    bot.reply_to(message, 'Вы можете узнать ваш id командой /id')

# Response to photo message
@bot.message_handler(content_types=["photo"]) # обрабатываем только картинки
def img_processing(message):

    #user_id = message.from_user.id
    user_id = message.chat.id

    # Соединяем путь с именем json файла
    user_img = str(user_id) + '.png'
    img_path = os.path.join(script_path, user_img)

    file_info = bot.get_file(message.photo[-1].file_id) # получаем информацию о файле
    file = bot.download_file(file_info.file_path) # скачиваем файл

    print(img_path)

    with open(img_path, "wb") as f: # открываем файл для записи
        f.write(file) # записываем содержимое


# Получить id
@bot.message_handler(commands=['id'])
def get_id(message):
    id = message.from_user.id
    bot.reply_to(message, "Your id: " + str(id))


# Генерация изображения с помощью juggernaut
@bot.message_handler(commands=['img'])
@restricted_access
def generate_image(message):
    print(message.text)
    if message.text.strip() == '/img':
        bot.reply_to(message, 'Add your request after /img command. Like /img beautiful sunset')
    else:
        mk_juggernaut(message)

# Generate textrure
@bot.message_handler(commands=['tex'])
@restricted_access
def generate_tex(message):
    if message.text.strip() == '/tex':
        bot.reply_to(message, 'Add your request after /tex command. Like /tex concrete wall')
    else:
        mk_texture(message)

@bot.message_handler(commands=['mat'])
@restricted_access
def generate_material(message):
    mk_mat(message)

# Make texture variation
@bot.message_handler(commands=['var'])
@restricted_access
def generate_tex_variation(message):
    mk_texture(message, 1)

@bot.message_handler(commands=['up'])
@restricted_access
def upscale(message):
    mk_upscale(message)

@bot.message_handler(commands=['upmat'])
@restricted_access
def upscale_material(message):
    mk_upscale_mat(message)

# Create stylezed inst
@bot.message_handler(commands=['inst'])
@restricted_access
def generate_inst(message):
    print(message.text)
    if message.text.strip() == '/inst':
        bot.reply_to(message, 'Add your request after /inst command. Like /inst Cartoon style')
    else:
        mk_inst(message)  


# Сохраняем картики в виде файла (чтобы не терялось качество)
@bot.message_handler(content_types=["document"]) # обрабатываем только документы
@restricted_access
def summarize_docs(message):
    user_id = message.from_user.id

    # Соединяем путь с именем json файла
    user_doc= str(user_id)
    doc_path = os.path.join(script_path, user_doc)

    file_info = bot.get_file(message.document.file_id) # получаем информацию о файле по его идентификатору
    downloaded_file = bot.download_file(file_info.file_path) # загружаем файл по его пути
    file_name = message.document.file_name # получаем имя файла из сообщения
    os.makedirs(doc_path, exist_ok=True)

    # extract extension
    split_tup = os.path.splitext(file_name)
    file_extension = split_tup[1]

    if file_extension == '.txt':
        file_path = os.path.join(doc_path, 'source.txt')
        with open(file_path, 'wb') as f: # открываем файл для записи в бинарном режиме
            f.write(downloaded_file) # записываем содержимое загруженного файла
        print('Document saved: ', file_path)
        bot.reply_to(message, 'Документ сохранен: ' + file_path) # отправляем ответное сообщение пользователю

    elif file_extension == '.xlsx':
        file_path = os.path.join(doc_path, 'source.xlsx')
        with open(file_path, 'wb') as f: # открываем файл для записи в бинарном режиме
            f.write(downloaded_file) # записываем содержимое загруженного файла
        print('Document saved: ', file_path)
        bot.reply_to(message, 'Документ exel сохранен: ' + file_path) # отправляем ответное сообщение пользователю
    elif file_extension in imagetypes:
        file_path = doc_path +  '.png'
        with open(file_path, 'wb') as f: # открываем файл для записи в бинарном режиме
            f.write(downloaded_file) # записываем содержимое загруженного файла
        print('Document saved: ', file_path)
        #bot.reply_to(message, 'Документ exel сохранен: ' + file_path) # отправляем ответное сообщение пользователю
    else:
        file_path = os.path.join(doc_path, message.document.file_name) # Specify the destination path
        with open(file_path, 'wb') as new_file:
            new_file.write(downloaded_file)
        bot.reply_to(message, 'Документ сохранен: ' + file_path) # Reply to the user

bot.polling()



